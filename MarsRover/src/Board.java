/*************************************************************************
 *  Compilation:  javac Board.java
 *  Execution:    java Board 
 *
 *  @author Ian Vurayai ianvurayai@gmail.com
 *
 *************************************************************************/

import java.io.IOException;
import java.util.ArrayList;

public class Board {
	public final int maxX;
	public final int maxY;
	public ArrayList <Rover> allRovers =  new ArrayList<Rover>();
    
	/**
	 * Board constructor 
	 */
	public Board(int maxX, int maxY){
		this.maxX = maxX;
		this.maxY = maxY;
	}
	/**
	 * function to add rovers to board
	 */
	public void addRover(Rover rover, String ins) throws IOException{
		rover.setMaxX(this.maxX);
		rover.setMaxY(this.maxY);
		rover.crush();

		for(char c : ins.toCharArray()) {
			
			for(int i =0; i<allRovers.size(); i++){
				if(rover.samePosition(allRovers.get(i))) {
					throw new IOException();
				}
			}

			if (c == 'L'){
				rover.faceLeft();
			} else if(c == 'R'){
				rover.faceRight();
			} else if(c == 'M'){
				rover.pointFoward();
			}else{
				throw new IOException();
			}
        }
		    for(int i =0; i<allRovers.size(); i++){
			  if(rover.samePosition(allRovers.get(i))) {
				  throw new IOException();
			  }
			} 
		//}



        allRovers.add(rover);
		rover.toString();
	}

	public static void main(String[] args) throws IOException {
		Board mars = new Board(5, 5);
		mars.addRover(new Rover(1, 2, 'N'), "LMLMLMLMM");
		mars.addRover(new Rover(3, 3, 'E'), "MMRMMRMRRM");

	}
}
