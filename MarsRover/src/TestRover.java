/*************************************************************************
 *  Compilation:  javac Rover.java
 *  Execution:    java Rover 
 *
 *  @author Ian Vurayai ianvurayai@gmail.com
 *
 *************************************************************************/

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class TestRover {

	@Test
	public void correctRover1() throws IOException {
		Rover rover = new Rover(5,5,1, 2, 'N');
		rover.faceLeft();
		rover.pointFoward();
		rover.faceLeft();
		rover.pointFoward();
		rover.faceLeft();
		rover.pointFoward();
		rover.faceLeft();
		rover.pointFoward();
		rover.pointFoward();
		assert(rover.toString().equals("1 3 N"));
	}

	@Test
	public void correctRover2() throws IOException {
		Rover rover = new Rover(5,5,3, 3, 'E');
		rover.pointFoward();
		rover.pointFoward();
		rover.faceRight();
		rover.pointFoward();
		rover.pointFoward();
		rover.faceRight();
		rover.pointFoward();
		rover.faceRight();
		rover.faceRight();
		rover.pointFoward();
		assert(rover.toString().equals("5 1 E"));	
	}


	@Test
	public void correctRover3() throws IOException {
		Rover rover = new Rover(1, 2, 'N');
		rover.faceLeft();
		rover.pointFoward();
		rover.faceLeft();
		rover.pointFoward();
		rover.faceLeft();
		rover.pointFoward();
		rover.faceLeft();
		rover.pointFoward();
		rover.pointFoward();
		assert(rover.toString().equals("1 3 N"));
	}

	@Test
	public void correctBoard() throws IOException {
		Board mars = new Board(5, 5);
		mars.addRover(new Rover(1, 2, 'N'), "LMLMLMLMM");
		mars.addRover(new Rover(3, 3, 'E'), "MMRMMRMRRM");
	}
	
	@Test(expected = IOException.class)
	public void wrongBoardInsrucrion() throws IOException {
		Board mars = new Board(5, 5);
		mars.addRover(new Rover(1, 2, 'N'), "KLMLMLMLMM");
	}
	
	@Test(expected = IOException.class)
	public void wrongRoverNegativeY() throws IOException {
		Rover rover = new Rover(3, -3, 'E');		
	}

	@Test(expected = IOException.class)
	public void wrongRoverNegativeX() throws IOException {
		Rover rover = new Rover(-3, 3, 'E');		
	}

	@Test(expected = IOException.class)
	public void wrongRoverPosiion() throws IOException {
		Rover rover = new Rover(3, 3, 'n');		
	}
	
	@Test
	public void areTwoRoversOnSamePosition() throws IOException {
		Rover rover = new Rover(3, 3, 'N');
		assert(rover.samePosition(new Rover(3, 3, 'N')));
	}

	@Test(expected = IOException.class)
	public void wrongRoverCrushedAfterOneMove() throws IOException {
		Rover rover = new Rover(3, 0, 'S');
		rover.pointFoward();
	}

	@Test(expected = IOException.class)
	public void wrongRoverCrushed() throws IOException {
		new Rover(5, 5, 6, 6, 'N');	
	}

}
