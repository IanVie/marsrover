/*************************************************************************
 *  Compilation:  javac Rover.java
 *  Execution:    java Rover 
 *
 *  @author Ian Vurayai ianvurayai@gmail.com
 *
 * Assumptions:
 * They cannot go of grid ==> crush
 * Two or more rovers can be on same x-y plane, its possible since we 
 * don't considering the z index
 *************************************************************************/
import java.io.IOException;
public class Rover {

	private int x;
	private int y;
	private char face;
	private  int maxX = Integer.MAX_VALUE;
	private  int maxY = Integer.MAX_VALUE;

	@SuppressWarnings("unused")
	private Rover(){

	}

	/**
	 * Rover constructor that takes x and y positions of the rover. And the facing direction
	 * @throws IOException if the x or y values are negative. And facing direction is none of NEWS letters
	 */
	public Rover(int x, int y, char face) throws IOException{
		if ((x >= 0 && y >= 0) && (face == 'N' || face == 'E' || face == 'W' || face == 'S')) {
			this.x = x;
			this.y = y;
			this.face = face;
		}
		else {
			throw new IOException();
		} 
	}

	/**
	 * Rover constructor that takes x and y positions of the rover, maxX and maxY positions of the 
	 * rover And the facing direction
	 * Throws exception if the x or y values are negative, if the x value is greater than maxX or y 
	 * value is greater than maxY. And facing direction is none of NEWS letters
	 */
	public Rover(int maxX, int maxY, int x, int y, char face) throws IOException{
		if ((x >= 0 && y >= 0) && (maxX >= x || maxY >= y) && (face == 'N' || face == 'E' || face == 'W' || face == 'S')) {
			this.x = x;
			this.y = y;
			this.face = face;
			this.maxX = maxX;
			this.maxY = maxY;
		}
		else {
			throw new IOException();
		} 
	}

	/**
	 * Methods that sets the maxX Value of the object. Facilitates the option of created rover before defining the space.
	 * @throws IOException 
	 */
	public void setMaxX(int xValue) throws IOException {
		if(xValue > 0 && this.x < xValue){
			this.maxX = xValue;
		}
		else {
			throw new IOException();
		}
	}

	/**
	 * Methods that sets the maxY Value of the object. Facilitates the option of created rover before defining the space.
	 * @throws IOException 
	 */
	public void setMaxY(int yValue) throws IOException {
		if(yValue > 0 && this.y < yValue ){
			this.maxY = yValue;
		}
		else{
			throw new IOException();
		}
	}

	/**
	 * returns the x cooadinates of the rover
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * returns the y cooadinates of the rover
	 */
	public int getY(){
		return this.y;
	}
	
	/**
	 * Checks if two rovers are at same position. 
	 */
	public boolean samePosition(Rover obj){
		if (this.x== obj.getX() && this.y == obj.getY()){
			return true;
		}
		return false;
	}

	/**
	 * Changes the direction to face left from current position 
	 * @throws IOException if the face of Rover is not known
	 */
	public void faceLeft() throws IOException {
		if (this.face == 'N') {
			this.face = 'W';
		} else if (this.face == 'E') {
			this.face = 'N';
		}else if (this.face == 'S') {
			this.face = 'E';
		}else if (this.face == 'W') {
			this.face = 'S';
		} else{
			throw new IOException();
		}
	}

	/**
	 * Changes the direction to face right from current position 
	 * @throws IOException if the face of Rover is not known
	 */
	public void faceRight() throws IOException {
		if (this.face == 'N') {
			this.face = 'E';
		} else if (this.face == 'W') { 
			this.face = 'N';
		} else if (this.face == 'S') {
			this.face = 'W';
		} else if (this.face == 'E') {
			this.face = 'S';
		}else {
			throw new IOException();
		}
	}

	/**
	 * Checks if the rover has not crushed, the rover can only crush 
	 * @throws IOException if its out of the given space boundary 
	 */
	public void crush() throws IOException{
		if(this.x < 0 || this.x > this.maxX || this.y < 0 || this.y > this.maxY) {
			throw new IOException();
		}
	}

	/**
	 * makes the rover move one step left.
	 * @throws IOException if the rover crushes, out of defined space
	 */
	public void decX() throws IOException {
		this.x = this.x - 1;
		crush();
	}

	/**
	 * makes the rover move one step down.
	 * @throws IOException if the rover crushes, out of defined space
	 */
	public void decY() throws IOException {
		this.y = this.y - 1;
		crush();
	}

	/**
	 * makes the rover move one step right.
	 * @throws IOException if the rover crushes, out of defined space
	 */
	public void incX() throws IOException {
		this.x = this.x + 1;
		crush();
	}

	/**
	 * makes the rover move one step up.
	 * @throws IOException if the rover crushes, out of defined space
	 */
	public void incY() throws IOException {
		this.y = this.y + 1;
		crush();
	}

	/**
	 * Decides where the object given the current position
	 * @throws IOException if the position does not exist
	 */
	public void pointFoward() throws IOException {
		if (this.face == 'N') {
			incY();
		} else if (this.face == 'W') { 
			decX();
		} else if (this.face == 'S') {
			decY();
		} else if (this.face == 'E') {
			incX();
		} else {
			throw new IOException();
		}
	}

	/**
	 * Converts the rover object to string
	 */
	public String toString() {
		System.out.println(this.x + " "+ this.y + " " + this.face);
		return this.x + " "+ this.y + " " + this.face;
	}
}
